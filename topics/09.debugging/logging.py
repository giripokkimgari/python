#################
import logging
from logging.handlers import RotatingFileHandler

#configure the logging settings using the basicConfig method. Allows to specify the output format, logging level and other options .
#The logging module provides several logging levels, which allow you to filter log messages based on their severity.
logging.basicConfig(
    level = logging.DEBUG, # Set the logging level (DEBUG, INFO, WARNING, ERROR, CRITICAL)
    format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    filename = "app.log",  # Save logs to a file
    filemode="w"  #overwrite log file - use 'a' to append
)

#create a logger object
logger = logging.getLogger("my_app")

#For long-running applications, you may want to rotate log files periodically to prevent them from growing indefinitely.
#The logging.handlers module provides a RotatingFileHandler and a TimedRotatingFileHandler for this purpose.
#his code creates a RotatingFileHandler that rotates log files when they reach a maximum size of 1024 bytes and keeps up to 3 backup log files.
log_handler = RotatingFileHandler("app.log", maxBytes=1024, backupCount=3)

#To make the log messages use the RotatingFileHandler, you need to attach the log_handler to the logger. You can do this using the setHandler method:
logger.addHandler(log_handler)  #the log messages will be directed to the file "app.log" with log rotation functionality.

#log messages with different log levels
logger.debug("This is a debug message.")
logger.info("This is an informational message.")
logger.warning("This is a warning message.")
logger.error("This is an error message.")
logger.critical("This is a critical error message.")

# Demonstrate log level filtering
logger.setLevel(logging.WARNING)  # Set the logging level to WARNING
logger.debug("This debug message won't be shown.")
logger.error("This error message will be shown.")
