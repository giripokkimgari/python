###############
#The assert statement is a debugging aid that tests a condition. If the condition is False, an AssertionError exception is raised. 
#It is often used to check that certain conditions are met during development.


#n this example, the assert statement checks that both the width and height are greater than 0 before calculating the area. If the condition is False, it raises an AssertionError with the provided error message.
def calculate_area(width, height):
    assert width > 0 and height > 0, "Both width and height must be greater than 0"
    return width * height

result = calculate_area(5, -3)


#It's important to note that in production code, assertions are often disabled globally (using the -O or -OO command line switches) to improve performance.
#Therefore, assertions should not be used for error handling in production code; they are primarily a debugging tool.
