import csv 

def process_csv(input_file, output_file):
    try:
        #Read data from the input csv file
        with open(input_file, 'r') as file:
            reader = csv.reader(file)
            data = [row for row in reader]

        #Process the data (in this case, lets just capitalize all netries)
        processed_data = [[entry.upper() for entry in row] for row in data]

        #write the processed data to the output file
        with open(output_file, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerows(processed_data)

        print(f"Data processed successfully. Results saved to {output_file}")

    except FileNotFoundError:
        print("File not found. Please check the file path.")
    except Exception as e:
        print(f"An error occured: {e}")

process_csv('input_data.csv', 'output_data.csv')