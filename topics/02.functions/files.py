##############
#function that renames files in a directory by adding a timestamp to their names. 
#We'll use the os and shutil modules to interact with the file system. 

import os
import shutil
from datetime import datetime

def rename_files_with_timestamp(directory_path):
    try:
        timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

        # List all files in the directory
        files = os.listdir(directory_path)

        for filename in files:
            if os.path.isfile(os.path.join(directory_path, filename)):
                new_name = f"{timestamp}_{filename}"
                new_path = os.path.join(directory_path, new_name)

                shutil.move(os.path.join(directory_path, filename), new_path)
                print(f"Renamed: {filename} -> {new_name}")

        print("File renaming completed.")

    except FileNotFoundError:
        print("Directory not found. Please check the directory path.")
    except Exception as e:
        print(f"An error occurred: {e}")

# Example usage
rename_files_with_timestamp('/path/to/your/directory')
