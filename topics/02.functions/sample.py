#can provide default values for function parameters. If values is ot provided when calling the function,, the default value is used.
#To execute a function, you need to call it by using its name followed by parentheses.
#If the function has parameters, you should provide values or arguments for those parameters when calling the function.
def wish(name="Guest"):
    print(f"Hello there!!! {name}")

wish("Giri") 

####function parameters
#a function can use areturn statement to send a value back to the caller.
#a function can return a single value or multiple values(as a tuple)
def add(a,b):
    result = a + b 
    return result
sum = add(4,5)
print(sum)

######scope of variables
#variables defined outside of any function are considered global and can be accessed from anywhere
#variables defined within a function are local to that function and cannot be accessed outside it

x = 10
def increment_x():
    x = 20
    x += 1
    print("Inside function:", x)

increment_x()
print("Outside function:", x)


#can use keyword arguments to specify values for function parameters by name, which can make the code more readable and explicit.
def greet(greeting, name):
    print(f"{greeting}, {name}!")

greet(name="Alice", greeting="Hello")


####Arbitrary arguments
#can use *args to pass a variable no.of non-keywords to a function. These args are erceived as tuple.

def print_items(*args):
    for item in args:
        print(item)
    
print_items("apple", "banana", "pomegranate")

####Arbitrary Keyword Arguments
#can use **kwargs to pass a variable number of keyword arguments to a function. These arguments are received as a dictionary.
def print_info(**kwargs):
    for key, value in kwargs.items():
        print(f"{key}: {value}")
print_info(name="Giri", age="30", city="singapore")

#recursion - func which calls itself
def factorial(n):
    if n == 0:
        return 1 
    else:
        return n * factorial(n - 1)
factorial(n=24)