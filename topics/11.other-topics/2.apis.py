import requests

def get_weather_data(city):
    url = f"https://api.example.com/weather/{city}"
    response = requests.get(url)

    if response.status_code == 200:
        data = response.json()
        return data
    else:
        return None 

if __name__ == "__main__":
    city = "Singapore"
    weather_data = get_weather_data(city)
    if weather_data:
        print(f"Weather in {city}: {weather_data['temperature']}°C, {weather_data['conditions']}")
    else:
        print(f"Failed to fetch weather data for {city}")