import os

def create_proj_dir(dir_name):
    proj_path = os.path.join("/tmp", dir_name)
    os.makedirs(proj_path, exist_ok=True)
    print("Created directory for {} at {}".format(dir_name, proj_path))

if __name__ == "__main__":
    dir_name = "my_directory"
    create_proj_dir(dir_name)