#For working with PDFs, the PyPDF2 library is commonly used.

import PyPDF2

# Open the PDF file
pdf_file = open('example.pdf', 'rb')

# Create a PDF reader object
pdf_reader = PyPDF2.PdfFileReader(pdf_file)

# Read a page
page = pdf_reader.getPage(0)
text = page.extractText()
print("Original Text:")
print(text)

# Open the PDF in append mode
pdf_writer = PyPDF2.PdfFileWriter()
pdf_writer.addPage(page)
pdf_writer.addPage(PyPDF2.PdfFileReader('another_page.pdf').getPage(0))

# Append text to the PDF
pdf_writer.addPage(page)
pdf_writer.addPage(PyPDF2.PdfFileReader('another_page.pdf').getPage(0))

# Save the updated PDF
with open('updated_example.pdf', 'wb') as updated_pdf:
    pdf_writer.write(updated_pdf)
