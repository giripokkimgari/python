#For working with Word documents, you can use the python-docx library. 

from docx import Document

# Open the Word document
doc = Document('example.docx')

# Read and update text
for paragraph in doc.paragraphs:
    if 'old_text' in paragraph.text:
        paragraph.text = paragraph.text.replace('old_text', 'new_text')

# Save the updated Word document
doc.save('updated_example.docx')
