#Web scraping involves extracting data from websites.
#using the requests library to fetch data from a webpage and the BeautifulSoup library for parsing HTML content.

import requests
from bs4 import BeautifulSoup

#URL of the webpage you want to scrape
url = "https:/nokia.com/"

#send an http GET request to the URL
response = requests.get(url)

#Check if the request was successful (status code 200)
if response.status_code == 200:
    #Parse the html content of the webpage
    soup = BeautifulSoup(response.text, "html.parser")

    #extract specific data
    title = soup.title.string
    paragraph = soup.find("p").text

    #print the xtracted data
    print("Title:", title)
    print("First paragraph:", paragraph)
else:
    print("Failed to ftech the webpage. Status code:", response.status_code)