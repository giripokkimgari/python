#You can use the openpyxl library to read and edit Excel spreadsheets. 

import openpyxl

#load the excel sheet
workbook = openpyxl.load_workbook("example.xlsx")

#select the work sheet
sheet = workbook['Sheet1']

#Read a cell
value = sheet['A1'].value
print("OriginalValue:", value)

#Update a cell
sheet['A1'] = "New cell"

#save the workbook
workbook.save("example.xlsx")