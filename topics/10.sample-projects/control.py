#To control the mouse and keyboard in Python, you can use various third-party libraries. 
#Here are examples using pyautogui for mouse and keyboard control.

import pyautogui
import time

# Move the mouse to specific coordinates
pyautogui.moveTo(100, 100, duration=1)

# Click the left mouse button
pyautogui.click()

# Type text
pyautogui.typewrite('Hello, World!', interval=0.1)

# Press and release a key
pyautogui.press('enter')

# Use hotkeys
pyautogui.hotkey('ctrl', 'c')  # Copy

# Wait for a few seconds
time.sleep(2)
