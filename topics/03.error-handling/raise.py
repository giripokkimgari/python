#########
def some_function(value):
    if value < 0:
        raise ValueError("Optional error message")
    return value

try:
    some_function(-5)  # This will raise a ValueError
except Exception as e:
    print(f"A value error occurred {e}\n")


#You can define custom exceptions by creating your own exception classes. 
#This allows you to handle specific error cases in a more structured way
class MyCustomException(Exception):
    pass

def custom_func(value):
    if value < 0:
        raise MyCustomException("Value must be non -negative")
    return value

try:
    custom_func(-5) #This will raise a MyCustomException
except MyCustomException as e:
    print("Custom Exception:", e)