##################
#Python provides a robust mechanism for handling errors, which includes try, except, finally and else blocks.



#########
#can use try block to enclose code that may raise an exception
#except block to catch and handle exceptions that occur within the try block
try:
    #code that may raise an exception
    result = 10 / 0
except ZeroDivisionError:
    #code to handle the exception
    print("Division by zero is not allowed.")


#######
#can use multiple except block to catch different types of exceptions
#else block is executed if no exceptions are raised within try block
try:
    value = int("123")
except ValueError:
    print("entered number is not valid")
except ZeroDivisionError:
    print("Division by zero is not allowed.")
else:
    print("Result:", value)


###########
#finally block is always executed, whether an exception raised or not. Its used for cleanup actions, such as closing file sor relasing resources.
try:
    file = open("sample.py", "r")
    content = file.read()
except FileNotFoundError:
    print("File not found")
finally:
    file.close() #ensure the file is closed, regardless of the outcome.


