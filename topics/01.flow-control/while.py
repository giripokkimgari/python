#########################################
#Used to execute block of code as long as a specified condition is true
#condition is checked before each iteration
#suitable for situations where the number of iterations is not known in advance or where you want to create interactive programs
#break statement allows to terminatethe loop before it reaches its natural end.
#continue statement allows to skip specific iterations withjout exiting the loop.
#########################################

##########################################
#We start with an infinite loop (while True) that continues until the user enters 'q' to quit.
#break statement to exit the loop if the user enters 'q'. This is a way to gracefully exit the loop.
#continue statement to skip the current iteration if the user enters anything other than a valid number. This allows the loop to continue without processing invalid input.
#If the input is a valid number, it is converted to an integer and added to the total.
#After the loop, we print the total of all valid numbers entered.
###########################################

#initialize variable
total = 0 

while True:
    user_input = input("Enter a number (or 'q' to exit): ")

    if user_input == 'q':
        break # exit the loop

    if not user_input.isdigit():
        print("Invcalid input. Please neter a valid number.")
        continue #skip current iteration and continue to the next iteration

    number = int(user_input)
    total += number  #total + number

print(f"Total of all valid numbers entered: {total}")