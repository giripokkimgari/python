#Prompt the user to enter a valid test score between 1 - 100
while True:
    try:
        score = float(input("Enter your test score: \n"))
        if 1 <= score <= 100:
           break
        else:
            print("Please enter a test score between 1 to 100")
    except ValueError:
        print("Please enter a valid integer number)")

#check the grade based on the user provided score
if 90 <= score <= 100:
    grade = "A"
elif 80 <= score <= 90:
    grade = "B"
elif 70 <= score <= 80:
    grade = "C"
else:
    grade = "Fail"

#check if the grade is A, B, or C. If it is, print a congratulatory message
if grade in ["A", "B", "C"]:
    print(f"Congratualations!!\n Your grade is {grade}")
else:
    print(f"Sorry, you have failed :(\n Your grade is {grade}\n Better luck next time.")
