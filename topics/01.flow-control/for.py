###################################
#Used to iterate over  asequence of items such as lists, tuples, strings or ranges
#Executes block of code for each item in the sequence
#Typlically used when you know the number of iterations in advance.
###################################

#generate amultiplication table between 2 and 5
for i in range(2, 5 + 1):
    print(f"Multiplication table for {i}:")
    for j in range(1, 10 + 1):
        result = i * j
        print(f"{i} x {j} = {result}")

#calculate the sum of all numbes in the table
sum = 0 
for i in range(2, 5 + 1):
    for j in range(1, 10 + 1):
        sum += i * j
print(sum)
print("\n")

#Iterate over list
fruits = ["Apple", "Banana", "Pomegranate"]
for fruit in fruits:
    print(f"I eat {fruit} daily morning")
print("\n")

#Iterate over range of numbers
for num in range(1, 6):
    print(num )
print("\n")

#Iterate over chars in a string
message = "Hello, world!"
for char in message:
    if char.isalpha():
        print(char)