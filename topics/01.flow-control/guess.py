##############
#
##############

import random

target_number = random.randint(1,100)

#initialize variables
attempts = 0
max_attempts = 5 #user has only 5 attempts to guess the number
user_name = input("Enter your name:")
print(f"Welcome to the guessing game {user_name}!!\n You have max attempts of 5 to guess the number")

while attempts < max_attempts:
    guessed_number = int(input("Enter your guessed number: "))

    if guessed_number < target_number:
        print("Your guess is to low. Try again!\n")
    elif guessed_number > target_number:
        print("Your guess is to high. Try again")
    else:
        print(f"Vola!!!!\n You have guessed the correct number, which is {target_number}")
        break #exit the loop since the correct number was guessed

    attempts += 1

if attempts == max_attempts:
    print(f"Sorry, you have run out of attempts.\n The correct number was {target_number}")