import os

print(os.getcwd())
os.chdir("/Users/giri/Study")
print(os.getcwd())

###
file = open("example.txt", "r")   #open a file for reading
content = file.read()
print(content)
file = open("example.txt", "w")   #open a file for writing
file = open("example.txt", "a")   #open a file for appending

file.close()   #its essential to close the file to free up system resources once you've finished working.


#reading files
#The with statement is recommended for working with files because it automatically takes care of closing the file after the block of code is executed. This ensures that the file is properly closed even if an exception occurs.
#reading the etire file
with open("example.txt", "r") as file:
    content = file.read()
    print(content)

#reading one line at a time
with open("example.txt", "r") as file:
    for line in file:
        print(line)

#Reading all lines into a list
with open("example.txt", "r") as file:
    lines = file.readlines()
    print(lines)


###writing to files
#to write to a file, you can use methods like write()
with open("example.txt", "w") as file:
    file.write("This is line 2 ")
    file.write("\n This is line 3 ")

with open("example.txt", "a") as file:
    file.write("New data to append.")
