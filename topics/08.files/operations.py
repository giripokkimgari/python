import os
import shutil

# Function to display the directory tree
def display_directory_tree(path, indent=""):
    print(indent + "📁 " + os.path.basename(path))
    for item in os.listdir(path):
        item_path = os.path.join(path, item)  #For each item in the directory, it constructs the full path to that item
        if os.path.isdir(item_path):
            display_directory_tree(item_path, indent + "  ")
        else:
            print(indent + "  📄 " + item)

# Create a directory for our operations
base_directory = "file_operations"
os.makedirs(base_directory, exist_ok=True)

# Absolute and relative file paths
file_path = os.path.join(base_directory, "sample.txt")  
print(f"file_path: {file_path}") #file_operations/sample.txt
relative_path = "file_operations/relative.txt"

# Writing to a file
with open(file_path, "w") as file:
    file.write("Hello, World!")

# Copying a file
shutil.copy(file_path, relative_path)

# Reading from a file
with open(relative_path, "r") as file:
    content = file.read()
    print("File Content:", content)

# Moving a file
new_location = os.path.join(base_directory, "sample.txt")
shutil.move(relative_path, new_location)

# Deleting a file
os.remove(new_location)

# Creating a subdirectory
subdirectory = os.path.join(base_directory, "subdir")  #Absolute path
os.makedirs(subdirectory, exist_ok=True)

# Display the directory tree
print("Directory Tree:")
display_directory_tree(base_directory)

# Deleting the entire directory and its contents
shutil.rmtree(base_directory)
