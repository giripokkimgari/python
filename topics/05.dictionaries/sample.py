################
#Dictionaries in Python are a versatile and fundamental data structure.
#They are used to store collections of data in a key-value format.
#Dictionaries are sometimes referred to as "associative arrays" or "hash maps" in other programming languages. 


#can create by enclosing key-value pairs within curly braces {} and separating them with colons :
my_dict = {"name": "giri", "age": "26",  "city": "Ongole"}

print(my_dict)

#Access the value associated with key name.
name = my_dict["name"]
age = my_dict["age"]
print(name + "\t" +  age)


#modifying dictionaries
my_dict["age"] = 31  #update the age
my_dict["country"] = "Singapore"  #add a new key-value pair
del my_dict["city"]  #Remove the key city and it associated value



#dict methods:
#python provides various methods for dictionaries such as keys(), values() and items() to access keys,values and key-value pairs.
keys = my_dict.keys()    #returns a list of keys
values = my_dict.values()  #returns a list of values
items = my_dict.items()   #retuns a list of key-value pairs
print(f"items: {items}" + f"keys: {keys}\n")


#Iterating  over dictionaries
#loop through dictionaries using for loop
for key in my_dict:
    value = my_dict[key]
    print(key, value)

#using items method to loop through key value pairs
for key, value in my_dict.items():
    print(key, value)


#check key existance in the dictionary
if "name" in my_dict:
    print("key 'name' exists in the dic")

#nested dictionaries

person = {
    "name": "Alice",
    "address": {
        "street": "123 Main street",
        "city": "Singapore"
    }
}

print(person["address"])