#build a basic contact manager that allows you to add, view, and search for contacts using dictionaries.

#initialize empty dictioanry
contacts = {}

#function to add contact
def add_contact(name, phone):
    contacts[name] = phone
    print(f"contact {name} added woth phone number: {phone}")

#function to remove contact
def remove_contact(name):
    if name in contacts:
        contacts.pop(name)
    else:
        print(f"key {name} not found to remove from dictionary")

#function to view all the contacts
def view_contacts():
    if contacts:
        print("Cobtacts:")
        for name, phone in contacts.items():
            print(f"Name: {name}, Phone: {phone}")
    else:
        print("Your contact list is empty")

#function to search a contact
def search_contact(name):
    if name in contacts:
        print(f"Contact found - Name: {name}, Phone: {contacts[name]}")
    else:
        print(f"Contact '{name}' not found in your list")



#main promgram loop

while True:
    print("\n Contact Manager")
    print("1. Add a contact")
    print("2. Remove a contact")
    print("3. View all contacts")
    print("4. Search for a contact")
    print("5. Quit")

    choice = input("Enter your choice from above options: ")

    if choice == '1':
        name = input("Enter the name to add: ")
        phone = input("Enter the phone number to add: ")
        add_contact(name, phone)
    elif choice == '2':
        name = input("Enter the name to remove: ")
        remove_contact(name)
    elif choice == '3':
        view_contacts()
    elif choice == '4':
        name = input("Enter name to search for: ")
        search_contact(name)
    elif choice == '5':
        print("Good bye!!!")
        break
    else:
        print("Invalid choice. Please choose from given options above.")