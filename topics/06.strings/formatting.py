#can concatenate strings and variables using the + operator.
name = "Alice"
age = 26
message = "Hello, " + name + ". You are " + str(age) + " years old."
print(message)

#F-strings are a concise and powerful way to format strings with variables and expressions. 
#place an f or F character before the string, and you can embed expressions using curly braces {}
name = "Alice"
age = 30
message = f"Hello, {name}. You are \"{age}\" years old."  #backslash before quote
print(message)


#String Formatting with .format() 
#The .format() method allows you to insert values into placeholders within a string.
name = "Giri"
age = 26
message = "Hello, {}. You are {} years old.".format(name, age)
print(message)

#This method is similar to C-style string formatting. It uses placeholders like %s for strings and %d for integers.
name = "Giri"
age = 26
message = "Hello, %s. You are %d years old." % (name, age)
print(message)



