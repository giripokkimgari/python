##########
#python provides a variety of builtin methods that you can use to manipulate and work with strings.


#uppercase and lowercase
text = "Hello, World"
upper_text = text.upper()
lower_text = text.lower()
print(f"upper_text: {upper_text}")
print(f"lower_text: {lower_text}")

#Removes leading and trailing whitespacxe chars from the string
message = "This is Giri from Singapore..   "
formatted_message = message.strip()
print(f"formatted message: {formatted_message}")

#replaces all occurances of 'old' and 'new' in the string
text = "Hello, world"
new_text = text.replace("Hello", "Hi")
print(new_text)


#splits a string into list of substrings based on the separator
text = "apple,banana,mango"
parts = text.split(",")
print(parts)
join_words = " ".join(text)


#check if string starts with specified prefix or ends with specified suffix
text = "Hello, world"
starts_with = text.startswith("Hello")
ends_with = text.endswith("world")
print(starts_with)
print(ends_with)


#check if the string consists of alphabetic characters, digits, alphanumeric characters, whitespace, etc.
text = "abcd123"
is_alpha = text.isalpha()
is_digit = text.isdigit()
is_alnum = text.isalnum()
is_space = text.isspace()
print(is_alpha)
print(is_digit)
print(is_alnum)
print(is_space)
