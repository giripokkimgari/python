###############
#Lists are a fundamental data structure in Python that allow you to store and manipulate collections of data.

#You can create a list by enclosing a sequence of values within square brackets [].
my_list = [1, 23, 54, 6, 7, 6, 36]

print(my_list[2]) #acess 3rd elements in the list

sub_list = my_list[1:4] #slicing aloows to create sublists by specifying a range of indices
print(sub_list)


my_list[2] = 100   #modify 3rd element

#list methods for common operations such as append, insert, remove, pop, sort and reverse
my_list.append(6)  #Adds 6 to the end of the list
my_list.insert(2, 8)  #Inserts 8 at index 2
my_list.remove(6)   # Remove the first occurence of 4
my_list.pop()  #Remove and return last element
my_list.sort()  #Sort the list in ascending order
my_list.sort(reverse=True)  ##Sort the list in descending order
my_list.reverse()  #Reverse the list

#Check if an element is in a list and find its index.
if 3 in my_list:
    index = my_list.index(3)


#LIsts can contain other lists, creating a nested structure
nested_list = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

copy_list = my_list.copy()


#List concatenation
list1 = [1, 2, 3]
list2 = [4, 5, 6]
combined_list = list1 + list2
print(combined_list)

#Unpack the element sof a list into separate variables using the
my_list = [1, 2, 4, 5]
a, b, c, d = my_list
print(a + b + c)