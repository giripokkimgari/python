#program to perform bsic list operations such as adding, removing and displaying items in the list.

#intialize empty list
ops_list = []


#function to add an item to the list
def add_item(item):
    ops_list.append(item)
    print(f"{item} added to the list")

#function to remove an item from the list
def remove_item(item):
    if item in ops_list:
       ops_list.remove(item)
       print(f"{item} removed from list")
    else:
        print(f"{item} is not in the list")


#function to display the current list
def display_list():
    # True if list contains any items
    if ops_list:
        print("current items in the list:")
        for item in ops_list:
            print(item)
    else:
        print("LIst is empty. Pls add some item to the list and try again this operation")


#main program loop
#provides a simple menu for adding, removing and displaying items in a list.
while True:
    print("\nOptions:")
    print("1. Add item to the list")
    print("2. Remove an item from the list")
    print("3. Display the list")
    print("4. Quit")

    choice = input("Enter your choice: ")
     
    if choice == '1':
        item = input("ENter the number to add: ")
        add_item(item)

    elif choice == '2':
        item = input("Enter the number to remove: ")
        remove_item(item)

    elif choice == '3':
        display_list()
        
    elif choice == '4':
        print("Exiting the program. Goodbye!")
    else:
        print(f"{choice} is inavlid choice. Please select a valid option")
    