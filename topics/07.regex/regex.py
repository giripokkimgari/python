import re
#search for a pattern
text = "The quick brown fox jumps over the lazy dog"
search_pattern = re.compile(r"fox|dog")
result = search_pattern.findall(text)
print(result)

#Match a date pattern
date = "11-07-2019"
date_pattern = re.compile(r"\d{2}-\d{2}-\d{4}")
result = date_pattern.findall(date)
if date_pattern.match(date):
    print("valid date: " + str(result))
else:
    print("Invalid date")

#Extract email addresses from text
text = "Send an email to john@example.com and mary@sample.com"
email_pattern = re.compile(r"[\w.-]+@[\w.-]+")
result = email_pattern.findall(text)
print(result)

# Regex Groups and the Pipe Character
# Matching either "apple" or "banana"
pattern = re.compile(r"(apple|banana)")
match = pattern.search("I enjoy both apple pie and banana bread.")
print("Matching 'apple' or 'banana':", match.group() if match else "Not found")

# Repetition in Regex Patterns and Greedy/Nongreedy Matching
# Matching any number of 'a's (greedy)
pattern = re.compile(r"a+")
match = pattern.search("aaaaaa")
print("Greedy matching of 'a+':", match.group() if match else "Not found")

# Matching the minimum number of 'a's (nongreedy)
pattern = re.compile(r"a+?")
match = pattern.search("aaaaaa")
print("Nongreedy matching of 'a+?':", match.group() if match else "Not found")

# Regex Character Classes and the findall() Method
# Matching any vowel
pattern = re.compile(r"[aeiou]")
vowels = pattern.findall("The quick brown fox jumps over the lazy dog.")
print("Vowels:", vowels)


# Regex Dot-Start and the Caret/Dollar Characters
# Matching lines starting with "The"
pattern = re.compile(r"^The.*")
match = pattern.search("The quick brown fox\nThe lazy dog")
print("Matching lines starting with 'The':", match.group() if match else "Not found")

# Regex sub() Method and Verbose Method
# Replacing 'apple' with 'fruit'
pattern = re.compile(r'apple')
text = "I have an apple and another apple."
replaced_text = pattern.sub('fruit', text)
print("After replacing 'apple' with 'fruit':", replaced_text)
