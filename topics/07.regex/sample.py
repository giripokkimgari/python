#Regular expressions are powerful tool for working with text data.
#They provide a flexible and concise way to search , match and manipulate strings based on specific patterns.

#Python has builtin module called re, to work with regular expressions.
import re

string = "Hello Giri!!!!"

#can compile regular expressions using the re.compile() function, which allows to resue the pattern multiple times.
search_pattern = re.compile(r'pattern_to_search')

#Matching and Searching
#Both functions return a match object if a match is found, or None if there's no match.
re.match(search_pattern, string)  #Searches for the pattern only at the beginning of the string.
re.search(search_pattern, string) #Searches for the pattern throughout the string.


#Basic Patterns:
# . (dot): Matches any character except a newline.
# ?: Matches zero or one occurrence of the preceding character or group.
# *: Matches zero or more occurrences of the preceding character or group.
# +: Matches one or more occurrences of the preceding character or group.
# []: Specifies a character class; for example, [0-9] matches any digit.
# | (pipe): Acts like an OR operator; for example, cat|dog matches either "cat" or "dog".

# Special Sequences:
# \d: Matches any digit (equivalent to [0-9]).
# \D: Matches any non-digit character.
# \w: Matches any word character (letters, digits, or underscores).
# \W: Matches any non-word character.
# \s: Matches any whitespace character.
# \S: Matches any non-whitespace character.

# Anchors:
# ^: Matches the start of a string.
# $: Matches the end of a string.

# Quantifiers:
# {n}: Matches exactly n occurrences of the preceding character or group.
# {n,}: Matches n or more occurrences of the preceding character or group.
# {n,m}: Matches between n and m occurrences of the preceding character or group.

# Groups:
# ( ... ): Creates a capturing group, allowing you to extract matched content.
# (?: ... ): Creates a non-capturing group.

# Escaping Special Characters:
# You can use \ to escape special characters to treat them as literal characters. For example, to search for a period (.), you would use '\.'.


