#A phone number and email scrapper

import re

text = "Contact us at phone: 123-456-7890 or email: example@email.com. For support, call (987) 654-3210."

#can compile regular expressions using the re.compile() function, which allows to resue the pattern multiple times.
phone_pattern = re.compile(r'\d{3}-\d{3}-\d{4}|\(\d{3}\) \d{3}-\d{4}')
email_pattern = re.compile(r'\S+@\S+')

#now use the above pattern to find the occurances in the given text with findall() method
phones = phone_pattern.findall(text)
emails = email_pattern.findall(text)

print("\nPhone NUmbers: ")
for phone in phones:
    print(phone)

print("\nEmail Addresses: ")
for email in emails:
    print(email)
